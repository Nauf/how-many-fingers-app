//
//  ViewController.swift
//  How Many Fingers
//
//  Created by Nauf on 06/02/16.
//  Copyright © 2016 Nauf. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
   
    @IBOutlet var labelAns: UILabel!
    @IBOutlet var userGuessTextField: UITextField!

    @IBAction func submit(sender: UIButton) {
    
           let userNo = userGuessTextField.text!
        
        let randomNumber = String(arc4random_uniform(6)) // to get random number
        
        
      
        
        if randomNumber == userNo {
        print("Correct guess")
        labelAns.text="Correct guess!"
        }
        else if userNo == ""{
            print("Please enter a number")
            labelAns.text="Please enter a number"
        }
            
        else
        {
        print("Wrong guess")
            labelAns.text="Wrong guess ! It was " + String(randomNumber)
        }
        
        userGuessTextField.text="";
    
    }//End of Submit

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

